import random
import pyautogui
import __main__
import time
from package2 import shooter

def markerPrint():
    print("marker")
    r = random.randint(1, 5)
    print("random is",r)
    
    # DEBUG:
    location = pyautogui.locateOnScreen("prototype/resource/imgs/steamIcon.png", grayscale=True, confidence=.55)
    # BUILD:
    # location = pyautogui.locateOnScreen(__main__.resource_path("..\\resource\imgs\steamIcon.png"), grayscale=True, confidence=.55)
    print(f"location is {location} ")
    pyautogui.click(location)
    time.sleep(2)
    
    # DEBUG:
    location2 = pyautogui.locateOnScreen("prototype/resource/imgs/chromeIcon.png", grayscale=True, confidence=.55)
    # BUILD:
    # location2 = pyautogui.locateOnScreen(__main__.resource_path("..\\resource\imgs\chromeIcon.png"), grayscale=True, confidence=.55)
   
    print(f"location2 is {location2} ")
    pyautogui.click(location2)
    
    shooter.shoot()
