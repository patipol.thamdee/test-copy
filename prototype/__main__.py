from package1 import marker
import os
import sys

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

def run():
    print("running")
    
    marker.markerPrint()

if __name__ == '__main__':
    run()